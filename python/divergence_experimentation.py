import tensorflow
import numpy as np

def create_divergence_kernels():
    """
        Create kernels in 3 different direction to calculate central differences
        The kernels will filter out the x, y, z direction vector
        Representing the gradients for each direction
    """
    kernel_x = np.zeros((3,3,3), dtype='float32')
    kernel_x[0,1,1] = 1 # filter x
    kernel_x[2,1,1] = -1 # filter x
    filter_x = tf.constant(kernel_x, dtype='float32')
    filter_x = tf.reshape(filter_x, [3, 3, 3, 1, 1])

    kernel_y = np.zeros((3,3,3), dtype='float32')
    kernel_y[1,0,1] = 1 # filter y
    kernel_y[1,2,1] = -1 # filter y
    filter_y = tf.constant(kernel_y, dtype='float32')
    filter_y = tf.reshape(filter_y, [3, 3, 3, 1, 1])

    kernel_z = np.zeros((3,3,3), dtype='float32')
    kernel_z[1,1,0] = 1 # filter z
    kernel_z[1,1,2] = -1 # filter z
    filter_z = tf.constant(kernel_z, dtype='float32')
    filter_z = tf.reshape(filter_z, [3, 3, 3, 1, 1])

    return (filter_x, filter_y, filter_z)

def calculate_gradient(image, kernel):
    """
        Calculate the gradient (edge) of an image using a predetermined kernel
    """
    # make sure it has 5 dimensions
    image = tf.expand_dims(image, 4)

    kernel_size = 3
    p = (kernel_size - 1) // 2
    image = tf.pad(image, [[0,0],[p,p],[p,p], [p,p],[0,0]], 'SYMMETRIC')

    conv = tf.nn.conv3d(image, kernel, strides=[1,1,1,1,1], padding='VALID')

    # remove the extra dimension
    conv = tf.squeeze(conv, 4)
    return conv

def calculate_divergence(u, v, w):
    """
        Calculate divergence for the corresponding velocity component
    """
    kernels = create_divergence_kernels()
    dudx = calculate_gradient(u, kernels[0])
    dvdy = calculate_gradient(v, kernels[1])
    dwdz = calculate_gradient(w, kernels[2])

    return (dudx, dvdy, dwdz)

def calculate_divergence_loss2(u, v, w, u_pred, v_pred, w_pred):
    (divpx, divpy, divpz) = calculate_divergence(u_pred, v_pred, w_pred)
    (divx, divy, divz) = calculate_divergence(u, v, w)

    return (divpx - divx) ** 2 + (divpy - divy) ** 2 + (divpz - divz) ** 2