from dataclasses import dataclass

@dataclass
class Permutation:
    diagonals : tuple
    rotations : tuple
    alt_rotations : list


def rotation_a(x):
    return (x[2], x[0], x[3], x[1])


def rotation_b(x):
    return (x[2], x[3], x[1], x[0])


def rotation_c(x):
    return (x[3], x[0], x[1], x[2])


def apply_transform(x, transform, n):
    if n > 1:
        return apply_transform(transform(x), transform, n-1)
    elif n == 1:
        return transform(x)
    else:
        return x


def find_permutation(target_diagonals, all_permutations):
    for i, permutation in enumerate(all_permutations):
        if permutation.diagonals == target_diagonals:
            return i
    return None


def discover_all_permutations():
    all_diagonals = []
    all_permutations = []
    for x in range(4):
        for y in range(4):
            for z in range(4):
                diagonals = (1, 2, 3, 4)
                diagonals = apply_transform(diagonals, rotation_a, x)
                diagonals = apply_transform(diagonals, rotation_b, y)
                diagonals = apply_transform(diagonals, rotation_c, z)
                rotations = (x, y, z)

                if diagonals not in all_diagonals:
                    all_diagonals.append(diagonals)
                    all_permutations.append(Permutation(diagonals, rotations, []))
                else:
                    permutation_index = find_permutation(diagonals, all_permutations)
                    if permutation_index is not None:
                        all_permutations[permutation_index].alt_rotations.append(rotations)
    if len(all_permutations) != 24:
        raise Exception('Not all permutations of S4 discovered')

    return all_permutations


def print_permutation(permutation):
    print(f'{permutation.diagonals} : {permutation.rotations}  alts: {permutation.alt_rotations}')


def main():
    all_permutations = discover_all_permutations()
    all_rotations = []
    for p in all_permutations:
        all_rotations.append(p.rotations)
    print(all_rotations)
        # print_permutation(p)



if __name__ == '__main__':
    main()







