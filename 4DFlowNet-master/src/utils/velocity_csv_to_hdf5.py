import numpy as np
import scipy.interpolate as sc
from scipy.spatial import KDTree
import argparse
import h5py

from . import h5utils
from . import format_velocity_csv

class CFDResult():
    def __init__(self, filepath, rounding, skip_header=0):
        self.read_csv(filepath, rounding, skip_header=skip_header)

    def read_csv(self, filepath, rounding, skip_header=0):
        """
            Read xyz coordinates and wss from CFD results (.csv)
            XYZ is converted to mm
        """
        format_velocity_csv.format_velocity_csv(filepath, skip_header)

        arr = np.genfromtxt(filepath, delimiter=',', names=True, skip_header=skip_header)
        
        x, y, z = arr['xcoordinate'], arr['ycoordinate'], arr['zcoordinate']

        # convert this to mm
        self.x = np.round(x * 1000, rounding)
        self.y = np.round(y * 1000, rounding)
        self.z = np.round(z * 1000, rounding)

        self.vx = arr['xvelocity']
        self.vy = arr['yvelocity']
        self.vz = arr['zvelocity']
        self.speed = arr['velocitymagnitude']
        
def get_minmax_arr(arr, skip_x):
    min_x, max_x = np.min(arr), np.max(arr)
    min_x, max_x = np.floor(min_x), np.ceil(max_x)

    # are these minus and plus skip_x necessary? removing them does not affect the result, though.
    min_x = min_x - skip_x
    max_x = max_x + skip_x
    x_arr = np.arange(min_x, max_x , skip_x)
    return x_arr, min_x, max_x


def convert_to_hdf5(input_path, output_path, header_size, dx, rounding):

    # Load the velocity data
    vel_cfd_res = CFDResult(input_path, rounding, skip_header=header_size)

    # Reshape xyz coordinates
    v_coords = np.stack((vel_cfd_res.x, vel_cfd_res.y, vel_cfd_res.z), axis=-1)
    # array with size [n, 3] where n is number of points

    # get min max coordinates
    x_arr, min_x, max_x = get_minmax_arr(v_coords[:,0], dx)
    y_arr, min_y, max_y = get_minmax_arr(v_coords[:,1], dx)
    z_arr, min_z, max_z = get_minmax_arr(v_coords[:,2], dx)
    # x_arr, y_arr, z_arr not used

    # yy, zz = np.meshgrid(y_arr, z_arr)
    xx, yy, zz = np.mgrid[min_x:max_x: dx, min_y:max_y:dx, min_z:max_z:dx]
    yy = np.asarray(yy)
    zz = np.asarray(zz)

    # --- get mask ---
    tree = KDTree(v_coords, leafsize=10)
    probe1 = np.stack((xx, yy, zz), axis=-1)
    distances, ndx = tree.query(probe1, k=1, distance_upper_bound=dx)
    # all values in distances are infinite.

    # consider point as a mask point when there is a point closer than half of dx
    mask = distances <= (dx / 2)

    # Prepare interpolator
    print("Preparing velocity interpolation")

    interpolator_vx = sc.LinearNDInterpolator(v_coords, vel_cfd_res.vx)
    interpolator_vy = sc.LinearNDInterpolator(v_coords, vel_cfd_res.vy)
    interpolator_vz = sc.LinearNDInterpolator(v_coords, vel_cfd_res.vz)

    print("Interpolating...")
    vx1 = interpolator_vx(xx,yy,zz)
    vy1 = interpolator_vy(xx,yy,zz)
    vz1 = interpolator_vz(xx,yy,zz)

    vx1 = np.nan_to_num(vx1)
    vy1 = np.nan_to_num(vy1)
    vz1 = np.nan_to_num(vz1)

    u_max = np.max(vx1)
    v_max = np.max(vy1)
    w_max = np.max(vz1)

    h5utils.save_to_h5(output_path, f"dx", (dx,dx,dx))
    h5utils.save_to_h5(output_path, f"origin", (min_x,min_y,min_z))
    h5utils.save_to_h5(output_path, f"u", vx1)
    h5utils.save_to_h5(output_path, f"v", vy1)
    h5utils.save_to_h5(output_path, f"w", vz1)
    h5utils.save_to_h5(output_path, f"u_max", u_max)
    h5utils.save_to_h5(output_path, f"v_max", v_max)
    h5utils.save_to_h5(output_path, f"w_max", w_max)
    h5utils.save_to_h5(output_path, f"mask", mask)


def main():
    parser = argparse.ArgumentParser(description='Generate HDF5 from input 3D velocity CSV. ')
    parser.add_argument('IN', help='Path to input CSV file')
    parser.add_argument('OUT', help='Path to output HDF5 file')
    parser.add_argument('--header',
        help='Size of header, the number of rows to skip in input CSV before reading data. Default: 5',  default=5)
    parser.add_argument('--dx', help='Isotropic spacing of points in output HDF5. Default: 0.6', default=0.6)
    parser.add_argument('--round', help='Number of decimal digits to round output values to. Default: 4', default=4)

    args = vars(parser.parse_args())

    input_path = args['IN']
    output_path = args['OUT']
    header_size = args['header']
    dx = args['dx']
    rounding = args['round']

    convert_to_hdf5(input_path, output_path, header_size, dx, rounding)

if __name__ == '__main__':
    main()