import numpy as np
import vtk
import SimpleITK as sitk
import h5py
import os
import scipy.ndimage as ndimage
import vtk.util.numpy_support as ns
import argparse

def load_mhd(filename):
    '''
        https://stackoverflow.com/questions/37290631/reading-mhd-raw-format-in-python
        This funciton reads a '.mhd' file using SimpleITK and return the image array, origin and spacing of the image.
    '''
    # Reads the image using SimpleITK
    itkimage = sitk.ReadImage(filename)

    # Convert the image to a  numpy array first and then shuffle the dimensions to get axis in the order z,y,x
    img = sitk.GetArrayFromImage(itkimage)
    return img


def scalar_to_vtk(scalar_array, spacing, filename, origin=None):
    """This function write a VtkImageData vti file from a numpy array.

    :param array: input array
    :type array: :class:`numpy.ndarray`
    :param origin: the origin of the array
    :type origin: array like object of values
    :param spacing: the step in each dimension
    :type spacing: array like object of values
    :param filename: output filename (.vti)
    :type filename: str
    """
    if origin is None:
        origin = (0, 0, 0)
    original_shape = scalar_array.shape

    array = scalar_array.ravel(order='F')
    array = np.expand_dims(array, 1)

    vtkArray = ns.numpy_to_vtk(num_array=array, deep=True,
                               array_type=ns.get_vtk_array_type(array.dtype))
    vtkArray.SetName("Magnitude")

    print('arr component', vtkArray.GetNumberOfComponents())
    print('nr tuples', vtkArray.GetNumberOfTuples())

    imageData = vtk.vtkImageData()
    imageData.SetOrigin(origin)
    imageData.SetSpacing(spacing)
    imageData.SetDimensions(original_shape)
    imageData.GetPointData().SetScalars(vtkArray)

    writer = vtk.vtkXMLImageDataWriter()
    writer.SetFileName(filename)
    writer.SetInputData(imageData)
    writer.Write()


# Based on: https://code.ornl.gov/rwp/javelin/commit/008c61d48384b975858f04de800a9556324d4d77
def vectors_to_vtk(vector_arrays, spacing, filename, origin=None):
    """This function write a VtkImageData vti file from a numpy array.

    :param array: input array
    :type array: :class:`numpy.ndarray`
    :param origin: the origin of the array
    :type origin: array like object of values
    :param spacing: the step in each dimension
    :type spacing: array like object of values
    :param filename: output filename (.vti)
    :type filename: str
    """
    if origin is None:
        origin = (0, 0, 0)

    (u, v, w) = vector_arrays
    original_shape = u.shape

    u = u.ravel(order='F')
    v = v.ravel(order='F')
    w = w.ravel(order='F')
    array = np.stack((u, v, w), axis=1)
    print(array.shape)

    # array is a shape of [n, 3]
    vtkArray = ns.numpy_to_vtk(num_array=array, deep=True,
                               array_type=ns.get_vtk_array_type(array.dtype))
    vtkArray.SetName("Velocity")

    print('arr component', vtkArray.GetNumberOfComponents())
    print('nr tuples', vtkArray.GetNumberOfTuples())

    imageData = vtk.vtkImageData()
    imageData.SetOrigin(origin)
    imageData.SetSpacing(spacing)
    imageData.SetDimensions(original_shape)
    imageData.GetPointData().SetScalars(vtkArray)

    u_arr = ns.numpy_to_vtk(num_array=u, deep=True,
                            array_type=ns.get_vtk_array_type(u.dtype))
    u_arr.SetName("U")
    imageData.GetPointData().AddArray(u_arr)

    v_arr = ns.numpy_to_vtk(num_array=v, deep=True,
                            array_type=ns.get_vtk_array_type(v.dtype))
    v_arr.SetName("V")
    imageData.GetPointData().AddArray(v_arr)

    w_arr = ns.numpy_to_vtk(num_array=w, deep=True,
                            array_type=ns.get_vtk_array_type(w.dtype))
    w_arr.SetName("W")
    imageData.GetPointData().AddArray(w_arr)

    writer = vtk.vtkXMLImageDataWriter()
    writer.SetFileName(filename)
    writer.SetInputData(imageData)
    writer.Write()


def get_vector_fields(input_filepath, columns, idx):
    with h5py.File(input_filepath, 'r') as hf:
        u = np.asarray(hf.get(columns[0])[idx])
        v = np.asarray(hf.get(columns[1])[idx])
        w = np.asarray(hf.get(columns[2])[idx])
    return u, v, w


def get_mask(input_filepath):
    with h5py.File(input_filepath, 'r') as hf:
        mask = np.asarray(hf.get('mask'))
    return mask


def vtkwriter(input_path, output_folder, mask_path):
    # might have to change this
    # "The spacing describes the physical size of each pixel."
    # spacing of my MRI data is 0.390625mm, 0.390625mm, 0.739394mm
    spacing = (2.4, 2.375, 2.375)  # Put your own spacing here

    # Load mask
    use_mask = False
    if mask_path is not None:
        print("loading mask")
        mask = load_mhd(mask_path)
        mask = np.transpose(mask, (0, 2, 1))  # WIth the pcmra mask in matlab, the axis was swapped
        print('mask', mask.shape)
        use_mask = True

    columns = ['u', 'v', 'w']
    # columns = ['mag_u','mag_v','mag_w']
    # Load HDF5
    with h5py.File(input_path, mode='r') as hdf5:
        data_nr = len(hdf5[columns[0]])

    # Build a vtk file per time frame
    for idx in range(0, data_nr):
        print('Processing index', idx)

        u, v, w = get_vector_fields(input_path, columns, idx)

        if use_mask:
            u *= mask
            v *= mask
            w *= mask

        output_path = os.path.join(output_folder, f'{idx}.vti')
        vectors_to_vtk((u, v, w), spacing, output_path)
        # scalar_to_vtk(u, spacing, output_filepath)
        # scalar_to_vtk(mask, spacing, output_filepath)

def main():
    parser = argparse.ArgumentParser(description='Generate a VTK (.VTI) file from velocity HDF5 to view in paraview')
    parser.add_argument('INPUT', help='Path to input HDF5')
    parser.add_argument('OUTPUT', help='Path to output VTK')
    parser.add_argument('--mask', required=False, help='Path to mask file for input. If not given, does not use a mask')

    args = vars(parser.parse_args())

    input_path = args['INPUT']
    output_folder = args['OUTPUT']
    mask_path = args['mask']

    # input_path = 'C:\\katze\\masters\\masters.git\\4DFlowNet-master\\mri_data\\mri_data_hr.h5'
    # output_folder = 'C:\\Users\\kzamb\\Desktop\\mri-data-hr\\'

    input_path = 'C:\\katze\\masters\\masters.git\\4DFlowNet-master\\mri_data\\network-output-hr-to-sr.h5'
    output_folder = 'C:\\Users\\kzamb\\Desktop\\network-output\\'


    vtkwriter(input_path, output_folder, mask_path)



if __name__ == '__main__':
    main()