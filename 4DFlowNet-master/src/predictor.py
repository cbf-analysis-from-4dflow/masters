import tensorflow as tf
import numpy as np
import time
import os
import argparse
from Network.SR4DFlowNet import SR4DFlowNet
from Network.PatchGenerator import PatchGenerator
from utils import prediction_utils
from utils.ImageDataset import ImageDataset



def prepare_network(patch_size, res_increase, low_resblock, hi_resblock):
    # Prepare input
    input_shape = (patch_size,patch_size,patch_size,1)
    u = tf.keras.layers.Input(shape=input_shape, name='u')
    v = tf.keras.layers.Input(shape=input_shape, name='v')
    w = tf.keras.layers.Input(shape=input_shape, name='w')

    u_mag = tf.keras.layers.Input(shape=input_shape, name='u_mag')
    v_mag = tf.keras.layers.Input(shape=input_shape, name='v_mag')
    w_mag = tf.keras.layers.Input(shape=input_shape, name='w_mag')

    input_layer = [u,v,w,u_mag, v_mag, w_mag]

    # network & output
    net = SR4DFlowNet(res_increase)
    prediction = net.build_network(u, v, w, u_mag, v_mag, w_mag, low_resblock, hi_resblock)
    model = tf.keras.Model(input_layer, prediction)

    return model


def predict(model_path, input_path, output_path):
    # Network settings
    patch_size = 24
    res_increase = 2
    batch_size = 24
    round_small_values = True
    low_resblock=8
    hi_resblock=4

    output_dir = os.path.split(output_path)[0]
    output_filename = os.path.split(output_path)[1]

    # Setting up
    pgen = PatchGenerator(patch_size, res_increase)
    dataset = ImageDataset()

    # Check the number of rows in the file
    nr_rows = dataset.get_dataset_len(input_path)
    print("Number of rows in dataset: " + str(nr_rows))

    print("Loading 4DFlowNet: " + str(res_increase) +  "x upsample")
    # Load the network
    network = prepare_network(patch_size, res_increase, low_resblock, hi_resblock)
    network.load_weights(model_path)

    if not os.path.isdir(output_dir):
        os.makedirs(output_dir)

    # loop through all the rows in the input file
    for nrow in range(0, nr_rows):
        print("\n--------------------------")
        print("\nProcessed (%i/%i) - %s" % (nrow+1, nr_rows, time.ctime()) )
        # Load data file and indexes
        dataset.load_vectorfield(input_path, nrow)
        print("Original image shape: " + str(dataset.u.shape))

        velocities, magnitudes = pgen.patchify(dataset)
        data_size = len(velocities[0])
        print("Patchified. Nr of patches: " + str(data_size) + " - " + str(velocities[0].shape))

        # Predict the patches
        results = np.zeros((0,patch_size*res_increase, patch_size*res_increase, patch_size*res_increase, 3))
        start_time = time.time()

        for current_idx in range(0, data_size, batch_size):
            time_taken = time.time() - start_time
            print("\rProcessed %i/%i  Elapsed: %.2f secs." % (current_idx, data_size, time_taken) , end='\r')
            # Prepare the batch to predict
            patch_index = np.index_exp[current_idx:current_idx+batch_size]
            sr_images = network.predict([velocities[0][patch_index],
                                    velocities[1][patch_index],
                                    velocities[2][patch_index],
                                    magnitudes[0][patch_index],
                                    magnitudes[1][patch_index],
                                    magnitudes[2][patch_index]])

            results = np.append(results, sr_images, axis=0)
        # End of batch loop
        time_taken = time.time() - start_time
        print("\rProcessed %i/%i  Elapsed: %.2f secs." % (data_size, data_size, time_taken), end='\r')

        for i in range (0,3):
            v = pgen._patchup_with_overlap(results[:,:,:,:,i], pgen.nr_x, pgen.nr_y, pgen.nr_z)

            # Denormalized
            v = v * dataset.venc
            if round_small_values:
                # print(f"Zero out velocity component less than {dataset.velocity_per_px}")
                # remove small velocity values
                v[np.abs(v) < dataset.velocity_per_px] = 0

            v = np.expand_dims(v, axis=0)
            prediction_utils.save_to_h5('%s/%s' % (output_dir, output_filename) , dataset.velocity_colnames[i], v, compression='gzip')

        if dataset.dx is not None:
            new_spacing = dataset.dx / res_increase
            new_spacing = np.expand_dims(new_spacing, axis=0)
            prediction_utils.save_to_h5(output_dir + '/' + output_filename, dataset.dx_colname, new_spacing, compression='gzip')
        print('\nCompleted\n')

def debug(input_path):
    dataset = ImageDataset()
    dataset.debug_image_dataset(input_path)


def main():
    parser = argparse.ArgumentParser(description='Upres and denoise velocity HDF5 of MRI data')
    parser.add_argument('MODEL', help='Trained 4dFlowNet model')
    parser.add_argument('INPUT', help='Path to input HDF5 file')
    parser.add_argument('OUTPUT', help='Path to output HDF5 file')
    parser.add_argument('--limitgpu', action='store_true', help='Limit execution to one GPU')
    args = vars(parser.parse_args())

    if args['limitgpu']:
        os.environ["CUDA_DEVICE_ORDER"] = "PCI_BUS_ID"
        os.environ["CUDA_VISIBLE_DEVICES"] = "0"
        print('Limiting execution to one GPU')
    else:
        print('No GPU limiting enabled')

    model_path = args['MODEL']
    input_path = args['INPUT']
    output_path = args['OUTPUT']
    predict(model_path, input_path, output_path)

if __name__ == '__main__':
    main()