from math import pi, atan, tan, sin, cos
import matplotlib.pyplot as plt
import numpy as np

class Fn:
    def __init__(self):
        self.x1 = None
        self.x2 = None

    def eval(self, _):
        return NotImplementedError('Eval not implemented for func')

    def connect(self, _previous_func, _initial):
        return NotImplementedError('Connect not implemented for func')

    def initial(self):
        return self.eval(self.x1)

    def final(self):
        return self.eval(self.x2)

class SinusoidFn(Fn):
    def __init__(self, x1=0.0, x2=None, length=None, amplitude=1.0, frequency=1.0, phase_shift=0.0, offset=0.0):
        super().__init__()
        if x2 is None and length is None:
            raise Exception('Must specify either x2 or length of sinusoid')
        self.x1 = x1
        self.x2 = x2
        self.length = length
        self.amplitude = amplitude
        self.frequency = frequency
        self.phase_shift = phase_shift
        self.angular_frequency = 2 * pi * frequency
        self.period = 1.0 / frequency
        self.offset = offset

    def __str__(self, tidy=False):
        if tidy:
            return f'{self.amplitude:.2e} sin(2π( {self.frequency:.2e} 𝒳 {self.phase_shift:+.2e} )) {self.offset:+.2e}\t\t[{self.x1}, {self.x2})'
        else:
            return f'{self.amplitude} * sin(2*pi * ( {self.frequency} * x {self.phase_shift:+} )) {self.offset:+}\t\t[{self.x1}, {self.x2})'

    def eval(self, x):
        return self.amplitude * sin(2 * pi * (self.frequency * x + self.phase_shift)) + self.offset

    def connect(self, previous_func, initial=False):
        self.x1 = previous_func.x2
        if previous_func.x2 is None and previous_func.length is not None:
            previous_func.x2 = previous_func.x1 + previous_func.length

        if self.length is not None:
            if self.x2 is None:
                self.x2 = self.x1 + self.length
            else:
                if self.x2 != self.x1 + self.length:
                    raise Exception('Conflicting values for length and x2')
        self.phase_shift -= self.x1
        if initial:
            return
        previous_final = previous_func.final()
        current_initial = self.initial()

        self.offset = previous_final - current_initial

class LineFn(Fn):
    def __init__(self, x1=None, y1=None, x2=None, y2=None):
        super().__init__()
        self.x1 = x1
        self.y1 = y1
        self.x2 = x2
        self.y2 = y2
        self.slope = None

    def __str__(self, tidy=False):
        neg_x1 = -self.x1
        if tidy:
            return  f'{self.slope:+.2e} (𝒳 {neg_x1:+} ) {self.y1:+}\t\t[{self.x1}, {self.x2})'
        else:
            return  f'{self.slope} * (x {neg_x1:+} ) {self.y1:+}\t\t[{self.x1}, {self.x2})'

    def connect(self, previous_func, _=None):
        if previous_func.x2 is None and previous_func.length is not None:
            previous_func.x2 = previous_func.x1 + previous_func.length
        self.x1 = previous_func.x2
        self.y1 = previous_func.final()
        self.slope = (self.y2 - self.y1) / (self.x2 - self.x1)

    def eval(self, x):
        if self.slope is None:
            return 0.0
        return self.slope * (x - self.x1 ) + self.y1

class PiecewiseFn:
    def __init__(self, initial_func=None, funcs=None, x1=None, x2=None, periodic=False):
        if periodic and (x1 is None or x2 is None):
            raise Exception('Period for periodic piecewise function must be specified by x1 and x2')
        if x1 is None:
            self.x1 = 0.0
        else:
            self.x1 = x1
        if x2 is None:
            self.x2 = 1.0
        else:
            self.x2 = x2

        self.periodic = periodic
        self.period = self.x2 - self.x1
        self.funcs= []
        if initial_func is not None:
            self.__iadd__(initial_func)
        if funcs is not None:
            for fn in funcs:
                self.__iadd__(fn)
        self.iter_index = 0

    def __iadd__(self, func):
        if len(self.funcs) == 0:
            dummy = LineFn(self.x1, y1=0, x2=self.x1, y2=0)
            func.connect(dummy, initial=True)
        else:
            func.connect(self.funcs[-1])
        self.funcs.append(func)
        return self

    def __iter__(self):
        self.iter_index = 0
        return self

    def __next__(self):
        if self.iter_index < len(self.funcs):
            func = self.funcs[self.iter_index]
            self.iter_index += 1
            return func
        else:
            raise StopIteration

    def eval(self, x):
        if self.periodic:
            x = x % self.period
        for func in self.funcs:
            if func.x1 <= x < func.x2:
                return func.eval(x)
        final_func = self.funcs[-1]
        if x == final_func.x2:
            return final_func.eval(x)
        raise Exception('Cannot evaluate outside of bounds (minimum x1 and maximum x2)')

def plot_piecewise(piecewise_function, xmin=0.0, xmax=1.0, ymin=-1.0, ymax=1.0, dx=0.001, print_fns=True, title=""):
    if print_fns:
        for func in piecewise_function:
            print(func)
    x = np.arange(xmin, xmax, dx)
    y = []
    for i in range(len(x)):
       y.append(piecewise_function.eval(x[i]))

    plt.plot(x,y,c='red', ls='', ms=5, marker='.')
    ax = plt.gca()
    ax.set_ylim([ymin, ymax])
    ax.set_title(title)
    ax.set_ylabel(r'volume flow rate ($m^3/s$)')
    ax.set_xlabel('time ($s$)')
    plt.show()

def prototype():
    basilar = PiecewiseFn(x1=0, x2=1, periodic=True)
    basilar += SinusoidFn(x1=0, x2=0.25, amplitude=2e-6, frequency=1.8, offset=0.6e-6)
    basilar += SinusoidFn(x2=0.5, amplitude=0.2e-6, frequency=0.8)
    basilar += LineFn(x2=1, y2=0.6e-6)

    left_ica = PiecewiseFn(x1=0, x2=1, periodic=True)
    left_ica += SinusoidFn(x1=0, x2=0.25, amplitude=4.2e-6, frequency=1.9, offset=1.6e-6)
    left_ica += SinusoidFn(x2=0.44, amplitude=0.5e-6, frequency=0.8)
    left_ica += LineFn(x2=1, y2=1.6e-6)

    right_ica = PiecewiseFn(x1=0, x2=1, periodic=True)
    right_ica += SinusoidFn(x1=0, x2=0.25, amplitude=4.6e-6, frequency=1.9, offset=1.6e-6)
    right_ica += SinusoidFn(x2=0.44, amplitude=0.5e-6, frequency=0.8)
    right_ica += LineFn(x2=1, y2=1.6e-6)

    # plot_piecewise(left_ica, xmin=0, xmax=1, ymin=1.5e-6, ymax=6.3e-6, dx=0.0005, title="Left internal carotid artery volumetric flow rate" )
    plot_piecewise(right_ica, xmin=0, xmax=1, ymin=1.5e-6, ymax=6.3e-6, dx=0.0005, title="Right internal carotid artery volumetric flow rate" )
    # plot_piecewise(basilar, xmin=0, xmax=1, ymin=0.5e-6, ymax=3e-6, dx=0.0005, title="Basilar artery volumetric flow rate" )


def main():
    prototype()

if __name__ == '__main__':
    main()