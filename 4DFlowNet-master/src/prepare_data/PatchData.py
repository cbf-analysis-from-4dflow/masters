import random as rnd
import numpy as np
import csv
import os

s4_rotations = [(0, 0, 1), (0, 0, 2), (0, 0, 3), (0, 1, 0), (0, 1, 1), (0, 1, 2), (0, 1, 3),
(0, 2, 0), (0, 2, 1), (0, 2, 2), (0, 2, 3), (0, 3, 0), (0, 3, 1), (0, 3, 2), (0, 3, 3), (1, 0, 0),
(1, 0, 1), (1, 0, 2), (1, 0, 3), (1, 2, 0), (1, 2, 1), (1, 2, 2), (1, 2, 3)]


def write_header(filename):
    if os.path.isfile(filename):
        return
    print(f'Creating patching file: {filename}')
    fieldnames = ['source', 'target','index', 'start_x', 'start_y', 'start_z', 'rotate', 'axis_1_rotation', 'axis_2_rotation', 'axis_3_rotation',  'coverage']
    with open(filename, mode='w', newline='') as csv_file:
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()


def generate_random_patches(lr_path, hr_path, csv_dir, index, n_train_locations, n_val_locations,
    n_benchmark_locations, binary_mask, patch_size, minimum_coverage, empty_patch_allowed, apply_all_rotation=True):
    empty_patch_counter = 0

    n_locations = n_train_locations + n_val_locations + n_benchmark_locations
    # foreach row, create 2 or 4 patches (determined by apply_all_rotation) at each of n locations
    j = 0
    not_found = 0
    while j < n_locations:
        if not_found > 100:
            print(f"Cannot find enough patches above {minimum_coverage} coverage, please lower the minimum_coverage")
            break
        if j >= n_train_locations+n_val_locations:
            csv_path = os.path.join(csv_dir, 'benchmark.csv')
        elif j >= n_train_locations:
            csv_path = os.path.join(csv_dir, 'validate.csv')
        else:
            csv_path = os.path.join(csv_dir, 'train.csv')

        can_still_take_empty_patch = empty_patch_counter < empty_patch_allowed
        print(f'patch number: {j} -- {csv_path} ')
        patch = PatchData(lr_path, hr_path, patch_size)
        
        # default, no rotation
        patch.create_random_patch(binary_mask, index)
        patch.calculate_patch_coverage(binary_mask, minimum_coverage)

        # before saving, do a check
        # print(patch.coverage)
        if patch.coverage < minimum_coverage:
            if can_still_take_empty_patch:
                
                print('Taking this empty one',patch.coverage)
                empty_patch_counter += 1
                
            else:
                # skip empty patch because we already have one
                not_found += 1
                continue

        patch.write_to_csv(csv_path)

        # apply ALL  rotation
        if apply_all_rotation:
            patch.rotate = 1
            for rotation in s4_rotations:
                patch.axis_1_rotation = rotation[0]
                patch.axis_2_rotation = rotation[1]
                patch.axis_3_rotation = rotation[2]
                patch.write_to_csv(csv_path)

                # /end of rotation idx
            # /end of plane
        else:
            patch.rotate = 1
            # do 1 random rotation
            random_plane = rnd.randint(1,3)
            if random_plane == 1:
                patch.axis_1_rotation = rnd.randint(1,3)
            elif random_plane == 2:
                patch.axis_2_rotation = rnd.randint(1,3)
            elif random_plane == 3:
                patch.axis_3_rotation = rnd.randint(1,3)
            patch.write_to_csv(csv_path)

        # break
        j += 1

    # /end of while n_patch
    
class PatchData:
    def __init__(self, source_file, target_file, patch_size):
        self.patch_size = patch_size

        self.source_file = source_file
        self.target_file = target_file
        self.idx = None
        self.start_x = None
        self.start_y = None
        self.start_z = None
        self.rotate = 0
        self.axis_1_rotation = 0
        self.axis_2_rotation = 0
        self.axis_3_rotation = 0
        self.coverage = 0

    def create_random_patch(self, u, index):
        self.idx = index
        self.start_x = rnd.randrange(0, u.shape[0] - self.patch_size + 1) 
        self.start_y = rnd.randrange(0, u.shape[1] - self.patch_size + 1) 
        self.start_z = rnd.randrange(0, u.shape[2] - self.patch_size + 1) 

    def set_patch(self, index, x, y, z):
        self.idx = index
        self.start_x = x
        self.start_y = y
        self.start_z = z

    def calculate_patch_coverage(self, binary_mask, minimum_coverage=0.2):
        patch_region = np.index_exp[self.start_x:self.start_x+self.patch_size, self.start_y:self.start_y+self.patch_size, self.start_z:self.start_z+self.patch_size]
        patch = binary_mask[patch_region]

        self.coverage = np.count_nonzero(patch) / self.patch_size ** 3
        self.coverage = np.round(self.coverage * 1000) / 1000 # round the number to 3 decimal digit


    def write_to_csv(self, output_filename):
        fieldnames = ['source', 'target', 'index', 'start_x', 'start_y', 'start_z', 'rotate', 'axis_1_rotation', 'axis_2_rotation', 'axis_3_rotation',  'coverage']
        with open(output_filename, mode='a', newline='') as csv_file:
            writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
            writer.writerow({'source': self.source_file, 'target': self.target_file, 'index': self.idx, 
            'start_x': self.start_x, 'start_y': self.start_y, 'start_z': self.start_z,
            'rotate': self.rotate, 'axis_1_rotation': self.axis_1_rotation,'axis_2_rotation': self.axis_2_rotation,
            'axis_3_rotation': self.axis_3_rotation, 'coverage': self.coverage})