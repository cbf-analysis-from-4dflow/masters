import os
import sys

vel_only_model_path = '../current-models/velocity-only-4p84/4DFlowNet-best.h5'
grad_loss_model_path = '../current-models/gradient-loss-2p46-1weight/4DFlowNet-best.h5'
div_loss_model_path = '../current-models/divergence-loss-2p94-0p3weight/4DFlowNet-best.h5'

mri_data_folder = '../../../masters-datastore/mri_data/'
output_folder = '../../../masters-datastore/network_output/'

trainer_call = ['python', 'predictor.py']

def vel_only_output_path(input_path):
    input_path = input_path.split('.h5')[0]
    patientcode = input_path.split(mri_data_folder)[1]

    return f'{output_folder}{patientcode}_velonly.h5'

def grad_loss_output_path(input_path):
    input_path = input_path.split('.h5')[0]
    patientcode = input_path.split(mri_data_folder)[1]

    return f'{output_folder}{patientcode}_gradloss.h5'

def div_loss_output_path(input_path):
    input_path = input_path.split('.h5')[0]
    patientcode = input_path.split(mri_data_folder)[1]

    return f'{output_folder}{patientcode}_divloss.h5'


def trainer_call(model_path, input_path, output_path):
    return f'python predictor.py {model_path} {input_path} {output_path}'


mri_input_path = sys.argv[1]

vel_only_call = trainer_call(vel_only_model_path, mri_input_path, vel_only_output_path(mri_input_path))
grad_loss_call = trainer_call(grad_loss_model_path, mri_input_path, grad_loss_output_path(mri_input_path))
div_loss_call = trainer_call(div_loss_model_path, mri_input_path, div_loss_output_path(mri_input_path))

os.system(vel_only_call)
os.system(grad_loss_call)
os.system(div_loss_call)