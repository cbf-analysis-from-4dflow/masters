"""
    Original source: https://www.datacamp.com/community/tutorials/matplotlib-3d-volumetric-data
"""
import sys

import h5py
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
import numpy as np
from PIL import Image



def update_title(title, ax, total_slice):
    title_str = f' << Prev (J) - {viewer_title} {ax.index}/{total_slice-1} - Next (K) >>'
    ax.set_title(title_str)

def remove_keymap_conflicts(new_keys_set):
    for prop in plt.rcParams:
        if prop.startswith('keymap.'):
            keys = plt.rcParams[prop]
            remove_list = set(keys) & new_keys_set
            for key in remove_list:
                keys.remove(key)

def multi_slice_viewer(volume, slice_axis=0, color='jet', show_colorbar=True, clim=None, show_grid=False, title='slice', save=False, savedir = '.', origin="upper", rect=None):
    global viewer_title
    global save_mode, save_dir
    save_dir = savedir
    save_mode = save
    viewer_title = title

    remove_keymap_conflicts({'j', 'k'})
    fig, ax = plt.subplots()

    assert slice_axis < 3, "Axis not supported, slice_axis must be between 0 and 2"

    if slice_axis == 1:
        volume = np.transpose(volume, (1, 0, 2))
    elif slice_axis == 2:
        volume = np.transpose(volume, (2, 0, 1))

    ax.volume = volume
    ax.index = volume.shape[0] // 2

    if rect is not None:
        ax.add_patch(rect)

    # if clim is None:
    im1 = ax.imshow(volume[ax.index], cmap=color, clim=clim, origin=origin)

    if show_colorbar:
        fig.colorbar(im1)

    update_title(title, ax, volume.shape[0])
    fig.canvas.mpl_connect('key_press_event', process_key)
    plt.grid(show_grid)
    plt.show()

def process_key(event):
    fig = event.canvas.figure
    ax = fig.axes[0]
    if event.key == 'j':
        previous_slice(ax)
    elif event.key == 'k':
        next_slice(ax)
    fig.canvas.draw()
    
    if save_mode:
        plt.savefig('{}/{}{}.png'.format(save_dir, 'img', ax.index), dpi=150)

def previous_slice(ax):
    volume = ax.volume
    ax.index = (ax.index - 1) % volume.shape[0]  # wrap around using %
    ax.images[0].set_array(volume[ax.index])
    
    update_title(viewer_title, ax, volume.shape[0])
    

def next_slice(ax):
    volume = ax.volume
    ax.index = (ax.index + 1) % volume.shape[0]
    ax.images[0].set_array(volume[ax.index])

    update_title(viewer_title, ax, volume.shape[0])


def run_multi_slice_viewer(path, velocity_component, slice_axis, color='jet', threshold=0, rect=None):
    with h5py.File(path, 'r') as hf:
        if velocity_component == 'mag':
            u = np.asarray(hf.get('u')[0])
            v = np.asarray(hf.get('v')[0])
            w = np.asarray(hf.get('w')[0])
            velocities = np.sqrt(np.power(u, 2) + np.power(v, 2) + np.power(w, 2))
            velocities = np.where(velocities < threshold, 0, velocities)

        else:
            velocities = np.asarray(hf.get(velocity_component)[0])
    print(velocities.shape)
    multi_slice_viewer(velocities, color=color, slice_axis=slice_axis, rect=rect)

def main():

    nr_path = r'C:\katze\masters\masters-datastore\mri_data\0152_F1_MRI_NR.h5'
    velonly_path = r'C:\katze\masters\masters-datastore\network_output\0152_F1_MRI_SR_velonly.h5'
    gradloss_path = r'C:\katze\masters\masters-datastore\network_output\0152_F1_MRI_SR_gradloss.h5'
    divloss_path = r'C:\katze\masters\masters-datastore\network_output\0152_F1_MRI_SR_divloss.h5'


    velocity_component = 'mag'
    slice_axis = 0
    #  0: axial  1: coronal 2: sagittal

    nr_refpoint_x = 94
    nr_refpoint_y = 83
    nr_size = 4

    sr_refpoint_x = nr_refpoint_x * 2
    sr_refpoint_y = nr_refpoint_y * 2
    sr_size = nr_size *2

    nr_rect = Rectangle((nr_refpoint_x,nr_refpoint_y),nr_size,nr_size,linewidth=1,edgecolor='r',facecolor='black', fill=False)
    sr_rect = Rectangle((sr_refpoint_x,sr_refpoint_y),sr_size,sr_size,linewidth=1,edgecolor='r',facecolor='black', fill=False)
    run_multi_slice_viewer(nr_path, velocity_component, slice_axis,threshold=0, rect=nr_rect)
    # run_multi_slice_viewer(velonly_path, velocity_component, slice_axis,threshold=0, rect=sr_rect)

if __name__ == '__main__':
    main()