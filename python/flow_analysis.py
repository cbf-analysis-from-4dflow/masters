from dataclasses import dataclass
from typing import List
import csv

import h5py
import numpy as np
import matplotlib.pyplot as plt

np.seterr(all='raise')


@dataclass
class Point:
    """
    A single point within an HDF5 image
    """

    x: int
    y: int
    z: int
    u: float = None
    v: float = None
    w: float = None
    mag: float = None

    def __str__(self):
        return f'({self.x}, {self.y}, {self.z}) : [{self.u}, {self.v}, {self.w} | {self.mag}]'

    def same_location(self, other):
        """
        Checks if two points have the same location. Does not check for equality in other fields.
        """
        return (self.x == other.x) and (self.y == other.y) and (self.z == other.z)



@dataclass
class Region:
    """
    A collection of Points, that may be a 2D slice, some 3D shape, or even multiple discontinuous areas of the image.
    Represents a region of interest in the image.
    u, v, w are averages of the respective velocity component for each Point in the Region,
    and mag (velocity magnitude) is calculated from u, v, w.
    """
    points: List[Point]
    u: float = None
    v: float = None
    w: float = None
    mag: float = None

    def overlapping_points(self, other):
        """
        Returns a 2-tuple where the first element is a list of overlapping points in this region,
        And the second is a list of overlapping points in the other region. As a point in both regions (i.e it has
        the same coordinates) may have other fields that differ between the regions, both versions are returned.
        """
        own_overlapping_points = []
        other_overlapping_points = []
        for own_point in self.points:
            for other_point in other.points:
                if own_point.same_location(other_point):
                    own_overlapping_points.append(own_point)
                    other_overlapping_points.append(other_overlapping_points)
        return own_overlapping_points, other_overlapping_points



@dataclass
class SquarePlane:
    """
    The return type of sqaure_plane(). Contains two Region() instances, a normal resolution
    version and super-resolution version of the same square plane.
    """

    normal_res : Region = None
    super_res : Region = None


def magnitude(a, b, c):
    """
    Calculate magnitude (euclidean norm) of vector with components a, b, c.
    The extra set of parantheses around the argument to np.sqrt might appear unnecessary, but
    it throws RuntimeWarnings otherwise.
    """
    return np.sqrt((np.power(a, 2) + np.power(b, 2) + np.power(c, 2)))



def set_regional_velocities(region):
    """
    Given a region where all its points have fully specified velocities, set the total for velocity components and magnitude.
    Returns region with this data set.
    """
    num_points = len(region.points)
    new_region = Region(points=region.points)
    new_region.u = sum([point.u for point in region.points]) / num_points
    new_region.v = sum([point.v for point in region.points]) / num_points
    new_region.w = sum([point.w for point in region.points]) / num_points
    new_region.mag = magnitude(new_region.u, new_region.v, new_region.w)

    return new_region


def analyze_region(region, hf, start_time, end_time):
    """
    Analyze a region in the HDF5 file (using file handle hf) from start_time until end_time (exclusively).
    Returns a list of Regions(), containing the same coordinates of points but at different timesteps,
    with all velocities set.

    """
    print(f'Analyzing region..\n')
    u = np.asarray(hf.get('u'))
    v = np.asarray(hf.get('v'))
    w = np.asarray(hf.get('w'))

    region_timesteps = []
    for t in range(start_time, end_time):
        analyzed_points = []
        for point in region.points:
            point_u = u[t][point.x][point.y][point.z]
            point_v = v[t][point.x][point.y][point.z]
            point_w = w[t][point.x][point.y][point.z]
            point_mag =  magnitude(point_u, point_v, point_w)
            analyzed_point = Point(x=point.x, y=point.y, z=point.z, u=point_u, v=point_v, w=point_w, mag=point_mag)
            analyzed_points.append(analyzed_point)
        analyzed_region = Region(points=analyzed_points)
        analyzed_region = set_regional_velocities(analyzed_region)
        region_timesteps.append(analyzed_region)

    return region_timesteps


def read_csv(path):
    rows = []

    with open(path) as file:
        reader = csv.DictReader(file)
        for row in reader:
            rows.append(row)
    return rows


def region_from_csv(csv_path):
    """
    Returns a region, without any associated velocities (for the whole region or for individual points)
    defined by a CSV.
    Returns: Region()
    """

    points = []
    csv_rows = read_csv(csv_path)
    for row in csv_rows:
        x = int(row['x'])
        y = int(row['y'])
        z = int(row['z'])
        point = Point(x=x, y=y, z=z)
        points.append(point)
    return Region(points=points)


def square_region(axis, refpoint, size, depth=1):
    """
    Define a square region in the specified axis.
    Returns Region(), with points set but velocities unset.
    axis is 0 for axial view, 1 for coronal view and 2 for sagitttal view.
    refpoint is a Point(). size is the number of points on either side to include
    Hence the total width and height of the Region() is 2 * size + 1.
    """
    region_points = []
    if axis == 0:
        for x in range(refpoint.x, refpoint + depth):
            for y in range(refpoint.y, refpoint.y + size):
                for z in range(refpoint.z, refpoint.z + size):
                    new_point = Point(x=x, y=y, z=z)
                    region_points.append(new_point)
    elif axis == 1:
        for y in range(refpoint.y, refpoint.y + depth):
            for x in range(refpoint.x, refpoint.x + size):
                for z in range(refpoint.z, refpoint.z + size):
                    new_point = Point(x=x, y=y, z=z)
                    region_points.append(new_point)
    elif axis == 2:
        for z in range(refpoint.z, refpoint.z + depth):
            for x in range(refpoint.x, refpoint.x + size):
                for y in range(refpoint.y, refpoint.y + size):
                    new_point = Point(x=x, y=y, z=z)
                    region_points.append(new_point)
    else:
        raise ValueError('Invalid axis. 0 is axial, 1 is coronal, 2 is saggital.')
    region = Region(points=region_points)
    return region


def square_plane(axis, nr_refpoint, nr_size):
    """
    Define a square plane in the specified axis, specified by the normal-res reference point and size.
    Returns SquarePlane(), containing a normal-res and a super-res Region, where the super-res Region is in the
    corresponding position in super-res data, accounting for a 2x upsampling factor.
    axis is 0 for axial view, 1 for coronal view and 2 for sagittal view.
    nr_refpoint (reference point) is a Point() the bottom left of the region.
    nr_size is the width and height of a square plane defined as beginning at that point.

    """
    sr_refpoint = Point(x=nr_refpoint.x * 2, y=nr_refpoint.y * 2, z=nr_refpoint.z * 2)
    sr_size = nr_size * 2

    nr_region = square_region(axis, nr_refpoint, nr_size, depth=1)
    sr_region = square_region(axis, sr_refpoint, sr_size, depth=2)

    return SquarePlane(normal_res=nr_region, super_res=sr_region)

def count_timesteps(hf):
    return len(hf['u'])


def network_comparison(comparison_plane, normal_res_h5, velonly_h5, gradloss_h5, divloss_h5, title, start_time=None,
                            end_time=None, analyze_nr=True):
    """
    Generate a plot with title comparing the three networks,  given by file handles velonly_h5, gradloss_h5, divloss_h5 at
    specified regoions.  from start_time until end_time.
    regions is a list

    """
    print(title)

    markersize = 20
    SMALL_SIZE = 8
    MEDIUM_SIZE = 14
    BIGGER_SIZE = 18

    plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
    plt.rc('axes', titlesize=MEDIUM_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=MEDIUM_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

    if start_time is None:
        start_time = 0

    if end_time is None:
        end_time = count_timesteps(normal_res_h5)

    velonly_timesteps = analyze_region(comparison_plane.super_res, velonly_h5, start_time, end_time)
    gradloss_timesteps = analyze_region(comparison_plane.super_res, gradloss_h5, start_time, end_time)
    divloss_timesteps = analyze_region(comparison_plane.super_res, divloss_h5, start_time, end_time)


    velonly_mag = [timestep.mag for timestep in velonly_timesteps]
    gradloss_mag = [timestep.mag for timestep in gradloss_timesteps]
    divloss_mag = [timestep.mag for timestep in divloss_timesteps]

    all_mags = velonly_mag + gradloss_mag + divloss_mag


    if analyze_nr:
        normal_res_timesteps = analyze_region(comparison_plane.normal_res, normal_res_h5, start_time, end_time)
        normal_res_mag = [timestep.mag for timestep in normal_res_timesteps]
        all_mags += normal_res_mag



    y_max = max(all_mags) * 1.1
    y_min = min(all_mags) * 0.9


    timesteps = list(range(start_time, end_time))

    if analyze_nr:
        plt.plot(timesteps, normal_res_mag, c='black', ls='', ms=markersize, marker='.', label='Normal res')
    plt.plot(timesteps, velonly_mag, c='red', ls='', ms=markersize, marker='.', label='Velocity MSE only')
    plt.plot(timesteps, gradloss_mag, c='blue', ls='', ms=markersize, marker='.',
             label='Velocity MSE and gradient loss ')
    plt.plot(timesteps, divloss_mag, c='green', ls='', ms=markersize, marker='.',
             label='Velocity MSE and divergence loss ')
    plt.legend(loc='upper right')
    ax = plt.gca()
    ax.set_ylim([y_min, y_max])
    ax.set_title(title)
    ax.set_ylabel('Velocity magnitude (m/s)')
    ax.set_xlabel('Timestep')
    ax.xaxis.get_major_locator().set_params(integer=True)
    plt.xticks(np.arange(min(timesteps), max(timesteps) + 2, 2))
    plt.show()

def compare_0180_F1_plane1():
    normal_res_h5_path = r'C:\Users\kzam437\Desktop\network_input\0180_F1.h5'
    velonly_h5_path = r'C:\Users\kzam437\Desktop\network_output\0180_F1_velonly.h5'
    gradloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0180_F1_gradloss.h5'
    divloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0180_F1_divloss.h5'
    title = 'Network comparison at acquisition plane 1 in MRI test case 1'
    axis = 0
    nr_refpoint = Point(x=45, y=84, z=67)
    nr_size = 4
    start_time = 0
    end_time = np.infty
    analyze_nr = True

    comparison_plane = square_plane(axis, nr_refpoint, nr_size)

    with h5py.File(normal_res_h5_path, 'r') as normal_res_h5, h5py.File(velonly_h5_path, 'r') as velonly_h5, \
            h5py.File(gradloss_h5_path, 'r') as gradloss_h5, h5py.File(divloss_h5_path, 'r') as divloss_h5:
        network_comparison(comparison_plane, normal_res_h5, velonly_h5, gradloss_h5, divloss_h5, title, analyze_nr=analyze_nr)

def compare_0180_F1_plane2():
    normal_res_h5_path = r'C:\Users\kzam437\Desktop\network_input\0180_F1.h5'
    velonly_h5_path = r'C:\Users\kzam437\Desktop\network_output\0180_F1_velonly.h5'
    gradloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0180_F1_gradloss.h5'
    divloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0180_F1_divloss.h5'
    title = 'Network comparison at acquisition plane 2 in MRI test case 1'
    axis = 1
    nr_refpoint = Point(x=45, y=87, z=65)
    nr_size = 4
    start_time = 0
    end_time = np.infty
    analyze_nr = True

    comparison_plane = square_plane(axis, nr_refpoint, nr_size)

    with h5py.File(normal_res_h5_path, 'r') as normal_res_h5, h5py.File(velonly_h5_path, 'r') as velonly_h5, \
            h5py.File(gradloss_h5_path, 'r') as gradloss_h5, h5py.File(divloss_h5_path, 'r') as divloss_h5:
        network_comparison(comparison_plane, normal_res_h5, velonly_h5, gradloss_h5, divloss_h5, title, analyze_nr=analyze_nr)


def compare_0090_F2():
    normal_res_h5_path = r'C:\Users\kzam437\Desktop\network_input\0090_F2.h5'
    velonly_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_velonly.h5'
    gradloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_gradloss.h5'
    divloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_divloss.h5'
    title = 'Test plot'
    axis = 0
    nr_refpoint = Point(x=34, y=95, z=87)
    nr_size = 4
    start_time = 0
    end_time = np.infty
    analyze_nr = True

    comparison_plane = square_plane(axis, nr_refpoint, nr_size)

    with h5py.File(normal_res_h5_path, 'r') as normal_res_h5, h5py.File(velonly_h5_path, 'r') as velonly_h5, \
            h5py.File(gradloss_h5_path, 'r') as gradloss_h5, h5py.File(divloss_h5_path, 'r') as divloss_h5:
        network_comparison(comparison_plane, normal_res_h5, velonly_h5, gradloss_h5, divloss_h5, title, analyze_nr=analyze_nr)

def compare_0090_F2_plane3():
    normal_res_h5_path = r'C:\Users\kzam437\Desktop\network_input\0090_F2.h5'
    velonly_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_velonly.h5'
    gradloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_gradloss.h5'
    divloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_divloss.h5'
    title = 'Network comparison at acquisition plane 3 in MRI test case 2'
    axis = 0
    nr_refpoint = Point(x=25, y=93, z=83)
    nr_size = 4
    start_time = 0
    end_time = np.infty
    analyze_nr = True

    comparison_plane = square_plane(axis, nr_refpoint, nr_size)

    with h5py.File(normal_res_h5_path, 'r') as normal_res_h5, h5py.File(velonly_h5_path, 'r') as velonly_h5, \
            h5py.File(gradloss_h5_path, 'r') as gradloss_h5, h5py.File(divloss_h5_path, 'r') as divloss_h5:
        network_comparison(comparison_plane, normal_res_h5, velonly_h5, gradloss_h5, divloss_h5, title, analyze_nr=analyze_nr)

def compare_0090_F2_plane4():
    normal_res_h5_path = r'C:\Users\kzam437\Desktop\network_input\0090_F2.h5'
    velonly_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_velonly.h5'
    gradloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_gradloss.h5'
    divloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0090_F2_divloss.h5'
    title = 'Network comparison at acquisition plane 4 in MRI test case 2'
    axis = 0
    nr_refpoint = Point(x=49, y=75, z=69)
    nr_size = 4
    start_time = 0
    end_time = np.infty
    analyze_nr = True

    comparison_plane = square_plane(axis, nr_refpoint, nr_size)

    with h5py.File(normal_res_h5_path, 'r') as normal_res_h5, h5py.File(velonly_h5_path, 'r') as velonly_h5, \
            h5py.File(gradloss_h5_path, 'r') as gradloss_h5, h5py.File(divloss_h5_path, 'r') as divloss_h5:
        network_comparison(comparison_plane, normal_res_h5, velonly_h5, gradloss_h5, divloss_h5, title, analyze_nr=analyze_nr)



def compare_0211_F0():
    normal_res_h5_path = r'C:\Users\kzam437\Desktop\network_input\0211_F0.h5'
    velonly_h5_path = r'C:\Users\kzam437\Desktop\network_output\0211_F0_velonly.h5'
    gradloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0211_F0_gradloss.h5'
    divloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0211_F0_divloss.h5'
    title = 'Test plot'
    axis = 0
    nr_refpoint = Point(x=34, y=95, z=87)
    nr_size = 4
    start_time = 0
    end_time = np.infty
    analyze_nr = True

    comparison_plane = square_plane(axis, nr_refpoint, nr_size)

    with h5py.File(normal_res_h5_path, 'r') as normal_res_h5, h5py.File(velonly_h5_path, 'r') as velonly_h5, \
            h5py.File(gradloss_h5_path, 'r') as gradloss_h5, h5py.File(divloss_h5_path, 'r') as divloss_h5:
        network_comparison(comparison_plane, normal_res_h5, velonly_h5, gradloss_h5, divloss_h5, title, analyze_nr=analyze_nr)

def compare_0211_F0_plane5():
    normal_res_h5_path = r'C:\Users\kzam437\Desktop\network_input\0211_F0.h5'
    velonly_h5_path = r'C:\Users\kzam437\Desktop\network_output\0211_F0_velonly.h5'
    gradloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0211_F0_gradloss.h5'
    divloss_h5_path = r'C:\Users\kzam437\Desktop\network_output\0211_F0_divloss.h5'
    title = 'Network comparison at acquistion plane 5 in MRI test case 3'
    axis = 0
    nr_refpoint = Point(x=47, y=149, z=81)
    nr_size = 4
    start_time = 0
    end_time = np.infty
    analyze_nr = True

    comparison_plane = square_plane(axis, nr_refpoint, nr_size)

    with h5py.File(normal_res_h5_path, 'r') as normal_res_h5, h5py.File(velonly_h5_path, 'r') as velonly_h5, \
            h5py.File(gradloss_h5_path, 'r') as gradloss_h5, h5py.File(divloss_h5_path, 'r') as divloss_h5:
        network_comparison(comparison_plane, normal_res_h5, velonly_h5, gradloss_h5, divloss_h5, title, analyze_nr=analyze_nr)



def main():
    sr_reg1 = compare_0180_F1_plane1()
    sr_reg2 = compare_0180_F1_plane2()

    points1, points2 = sr_reg1.overlapping_points(sr_reg2)
    for p in points1:
        print(p)


    # compare_0090_F2_plane3()
    # compare_0090_F2_plane4()
    # compare_0211_F0_plane5()

if __name__ == '__main__':
    main()
