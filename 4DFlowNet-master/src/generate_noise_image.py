import numpy as np
import os
import h5py
import random
from prepare_data.frequency_domain import get_complex_img
import scipy.ndimage as ndimage
from prepare_data.h5functions import save_to_h5

def get_complex_images(h5_path):
    """
    open a H5 file at h5_path, extract the phase and magnitude images for each direction, then return
    corresponding complex images for each direction.
    returns: tuple (u_complex, v_complex, w_complex), where each element of the tuple is an array
    with an element for each timestep
    """
    u_complex_imgs = []
    v_complex_imgs = []
    w_complex_imgs = []

    with h5py.File(h5_path, mode='r') as h5_file:
        data_count = len(h5_file.get("u"))  # number of timesteps

        for i in range(data_count):
            u = np.asarray(h5_file['u'][i])
            v = np.asarray(h5_file['v'][i])
            w = np.asarray(h5_file['w'][i])

            u_mag = np.asarray(h5_file['u_mag'][i])
            v_mag = np.asarray(h5_file['v_mag'][i])
            w_mag = np.asarray(h5_file['w_mag'][i])

            u_complex_img = get_complex_img(u_mag, u)
            v_complex_img = get_complex_img(v_mag, v)
            w_complex_img = get_complex_img(w_mag, w)

            u_complex_imgs.append(u_complex_img)
            v_complex_imgs.append(v_complex_img)
            w_complex_imgs.append(w_complex_img)
    return ( np.asarray(u_complex_imgs), np.asarray(v_complex_imgs), np.asarray(w_complex_imgs)  )


def main():
    hr_path = '../mri_data/mri_data_hr.h5'
    sr_path = '../mri_data/network-output-hr-to-sr.h5'

    differences_path = '../mri_data/noise_image.h5'

    hr_u_complex, hr_v_complex, hr_w_complex = get_complex_images(hr_path)
    sr_u_complex, sr_v_complex, sr_w_complex = get_complex_images(sr_path)

    data_count = len(hr_u_complex)

    u_complex_differences = []
    v_complex_differences = []
    w_complex_differences = []

    for i in range(data_count):
        u_complex_difference = hr_u_complex[i] - sr_u_complex
        v_complex_difference = hr_v_complex[i] - sr_v_complex
        w_complex_difference = hr_w_complex[i] - sr_w_complex

        u_complex_differences.append(u_complex_difference)
        v_complex_differences.append(v_complex_difference)
        w_complex_differences.append(w_complex_differences)





if __name__ == '__main__':
    main()