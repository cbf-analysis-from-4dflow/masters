import configparser

class ConfigReader:
    def __init__(self, path):
        self.path = path
        self.config = configparser.ConfigParser()
        self.config.read(path)

    def read(self, address, type_fn):
        section = address.split('.')[0]
        key = address.split('.')[1]
        str_value = self.config[section][key]
        if type_fn is bool:
            if str_value in ['0', 'false', 'False']:
                return False
            elif str_value in ['1', 'true', 'True']:
                return True
            else:
                raise Exception(f'Unknown value for key {address} : {str_value}')
        return type_fn(str_value)
