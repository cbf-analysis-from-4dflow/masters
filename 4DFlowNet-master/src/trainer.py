import numpy as np
from Network.PatchHandler3D import PatchHandler3D
from Network.TrainerController import TrainerController
import sys
import os
if sys.argv[1] == 'limitgpu':
    os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"
    os.environ["CUDA_VISIBLE_DEVICES"]="0"
    print('Limiting execution to one GPU')
elif sys.argv[1] == 'nolimit':
    print('Not limiting to GPU')
else:
    print('Unknown command-line argument')

def load_indexes(index_file):
    """
        Load patch index file (csv). This is the file that is used to load the patches based on x,y,z index
    """
    indexes = np.genfromtxt(index_file, delimiter=',', skip_header=True, dtype='unicode') # 'unicode' or None
    return indexes

def main():
    data_dir = '../data'
    
    # ---- Patch index files ----
    training_file = data_dir + '/train.csv'
    validate_file = data_dir + '/validate.csv'

    QUICKSAVE = True
    benchmark_file = data_dir + '/benchmark.csv'

    add_timestamp = False
    restore = True
    if restore:
        model_dir = "../models/4DFlowNet"
        model_file = "4DFlowNet-best.h5"

    # Hyperparameters optimisation variables
    initial_learning_rate = 2e-4
    n_epochs =  -5
    batch_size = 24
    mask_threshold = 0.6

    # Network setting
    network_name = '4DFlowNet'
    patch_size = 24
    res_increase = 2
    # Residual blocks, default (8 LR ResBlocks and 4 HR ResBlocks)
    low_resblock = 8
    hi_resblock = 4


    # Load data file and indexes
    trainset = load_indexes(training_file)
    valset = load_indexes(validate_file)
    
    # ----------------- TensorFlow stuff -------------------

    # TRAIN dataset iterator
    traindh = PatchHandler3D(data_dir, patch_size, res_increase, batch_size, mask_threshold)
    trainset =traindh.initialize_dataset(trainset, shuffle=True, n_parallel=None)

    # VALIDATION iterator
    valdh = PatchHandler3D(data_dir, patch_size, res_increase, batch_size, mask_threshold)

    valset = valdh.initialize_dataset(valset, shuffle=True, n_parallel=None)


    # # Bechmarking dataset, use to keep track of prediction progress per best model
    testset = None
    if QUICKSAVE and benchmark_file is not None:
        # WE use this bechmarking set so we can see the prediction progressing over time
        benchmark_set = load_indexes(benchmark_file)
        ph = PatchHandler3D(data_dir, patch_size, res_increase, batch_size, mask_threshold)
        # No shuffling, so we can save the first batch consistently
        testset = ph.initialize_dataset(benchmark_set, shuffle=False) 

    # ------- Main Network ------
    print("4DFlowNet patch size %i, lr %f, batch size %i" % (patch_size, initial_learning_rate, batch_size))
    network = TrainerController(patch_size, res_increase, initial_learning_rate, QUICKSAVE, network_name, low_resblock, hi_resblock)
    network.init_model_dir(add_timestamp)

    if restore:
        if network.restore_model(model_dir, model_file):
            print(f"Restored model {os.path.join(model_dir, model_file)}" )
            print("Learning rate", network.optimizer.lr.numpy())
        else:
            print('Could not restore model.')
            restore = False
    network.train_network(trainset, valset, n_epochs, testset, restore)


if __name__ == '__main__':
    main()