from utils.velocity_csv_to_hdf5 import convert_to_hdf5
from prepare_data.prepare_lowres_dataset import downsample_hr_hdf5
from prepare_data.prepare_patches import generate_patching_csv

def generate_training_data():
    # input / output paths
    # velocity_csv_path = 'interpolated_cfd.csv'
    hr_path = '../data/HR.h5'
    lr_path = '../data/LR.h5'
    csv_dir = '../data/'

    # additional parameters for convert_to_hdf5()
    header_size = 5
    dx = 0.6
    rounding = 4

    # additional parameters for downsample_hr_hdf5()
    downsample_rate = 2
    base_venc_multiplier = 1.1

    # additional parameters for generate_patching_csv()
    patch_size = 24
    n_train_locations = 22
    n_val_locations = 4
    n_benchmark_locations = 4
    max_empty_patches = 0
    all_rotation = 1
    mask_threshold = 0.4
    minimum_coverage = 0.035

    min_i = 301
    max_i = 351
    for i in range(min_i, max_i):
        print(f'convert_to_hd5 {i}')
        convert_to_hdf5(f'pipeline-input\{i}.csv', hr_path, header_size, dx, rounding)

    downsample_hr_hdf5(hr_path, lr_path, downsample_rate, base_venc_multiplier, mri=False)
    generate_patching_csv(hr_path, lr_path, csv_dir, patch_size, n_train_locations, n_val_locations,
        n_benchmark_locations, max_empty_patches, all_rotation, mask_threshold, minimum_coverage)

def downsample_mri_data():
    hr_path = '../mri_data/mri_data_hr.h5'
    lr_path = '../mri_data/mri_data_lr.h5'
    downsample_rate = 2
    base_venc_multiplier = 1.1
    downsample_hr_hdf5(hr_path, lr_path, downsample_rate, base_venc_multiplier, mri=True)

def main():
    # generate_training_data()
    downsample_mri_data()

if __name__ == '__main__':
    main()
