import numpy as np
import h5py
from . import PatchData as pd
import argparse
import os

def load_data(input_filepath):
    with h5py.File(input_filepath, mode = 'r' ) as hdf5:
        data_nr = len(hdf5['u'])

    indexes = np.arange(data_nr)
    print("Dataset: {} rows".format(len(indexes)))
    return indexes

def count_patches(path):
    with open(path) as f:
        lines = f.readlines()
        return len(lines) -1  # don't count the header

def generate_patching_csv(hr_path, lr_path, csv_dir, patch_size,  n_train_locations, n_val_locations,
      n_benchmark_locations, max_empty_patches, all_rotation, mask_threshold, minimum_coverage):
    # Load the data
    input_filepath = f'{lr_path}'
    file_indexes = load_data(input_filepath)

    # Prepare the CSV output
    train_path = os.path.join(csv_dir, 'train.csv')
    val_path = os.path.join(csv_dir, 'validate.csv')
    benchmark_path = os.path.join(csv_dir, 'benchmark.csv')
    pd.write_header(train_path)
    pd.write_header(val_path)
    pd.write_header(benchmark_path)

    # because the data is homogenous in 1 table, we only need the first data
    with h5py.File(input_filepath, mode = 'r' ) as hdf5:
        mask = np.asarray(hdf5['mask'][0])
    # We basically need the mask on the lowres data, the patches index are retrieved based on the LR data.
    print("Overall shape", mask.shape)

    # Do the thresholding
    binary_mask = (mask >= mask_threshold) * 1

    # Generate random patches for all time frames
    for index in file_indexes:
        print('Generating patches for row', index)
        pd.generate_random_patches(lr_path, hr_path, csv_dir, index,  n_train_locations, n_val_locations,
        n_benchmark_locations, binary_mask, patch_size, minimum_coverage, max_empty_patches, all_rotation)
    n_train_patches = count_patches(train_path)
    n_validate_patches = count_patches(val_path)
    n_benchmark_patches = count_patches(benchmark_path)
    print(f'Done. Generated {n_train_patches} training patches, {n_validate_patches} validate patches and {n_benchmark_patches} benchmark patches and saved them in {csv_dir}')



def main():
    parser = argparse.ArgumentParser('Generates CSV that describes patches for training.\n'
         'The total number of patches is npatch * 2 if allrotations is 0, and npatch * 4 otherwise. ')
    parser.add_argument('HR', help='Path to input high-resolution HDF5 file')
    parser.add_argument('LR', help='Path to input low-resolution HDF5 file')
    parser.add_argument('OUT', help='Path of folder where train.csv, validate.csv and benchmark.csv will be created')
    parser.add_argument('--patchsize', help='Isotropic size (in voxels) of patches.  Default: 16', default=16)
    parser.add_argument('--ntrain', help='Number of distinct patch locations for training. Default: 10', default=10)
    parser.add_argument('--nval', help='Number of distinct patch locations for validation. Default: 5', default=5)
    parser.add_argument('--ntest', help='Number of distinct patch locations for testing. Default: 5', default=5)
    parser.add_argument('--maxempty', help='Maximum number of empty patches allowed. Default: 0', default=0)
    parser.add_argument('--allrotations',
        help='Boolean. When 1: include all possible rotations for each patch, a total of 24 at each location. Default: 0 ', default=0)
    parser.add_argument('--maskthreshold', help='Threshold for non-binary mask. Default: 0.4', default=0.4)
    parser.add_argument('--mincoverage',
        help='Minimum fluid region within a patch. Any patch with less than this coverage will not be taken. Range: 0-1  Default: 0.05', default=0.05)

    args = vars(parser.parse_args())
    hr_path = args['HR']
    lr_path = args['LR']
    csv_path = args['OUT']
    patch_size = args['patchsize']
    n_train_locations = args['ntrain']
    n_val_locations = args['nval']
    n_test_locations = args['ntest']
    max_empty_patches = args['maxempty']
    all_rotations = args['allrotations']
    if all_rotations not in [0, 1]:
        raise argparse.ArgumentTypeError('--allrotations value must be 0 or 1')
    mask_threshold = args['mashthreshold']
    minimum_coverage = args['mincoverage']

    generate_patching_csv(hr_path, lr_path, csv_path, patch_size, n_train_locations, n_val_locations, n_test_locations,
        max_empty_patches, all_rotations, mask_threshold, minimum_coverage)


    if __name__ == '__main__':
        main()