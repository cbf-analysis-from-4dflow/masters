def format_velocity_csv(path, header_size):
    with open(path, 'r') as file:
        lines = file.readlines()
    with open(path, 'w') as file:
        for i, line in enumerate(lines):
            if i < header_size:
                file.write(',,,,,,,\n')
            elif i==header_size:
                file.write('xcoordinate, ycoordinate,zcoordinate,velocitymagnitude,xvelocity,yvelocity,zvelocity\n')
            else:
                file.write(line)


