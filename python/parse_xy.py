import string
from dataclasses import dataclass

@dataclass
class XYData:
    x : list
    y : list


def read_file(filename):
    text = ""
    with open(filename) as f:
        text = f.readlines()
    return text


def string_is_number(s):
    return s.replace('.', '').isnumeric()


def parse_line(line):
    line = line.split()
    for element in line:
        if not string_is_number(element):
            return None
    if len(line) != 2:
        return None
    return line


def parse(file_contents):
    x = []
    y = []
    for line in file_contents:
        parsed_line = parse_line(line)
        if parsed_line is not None:
            x.append(parsed_line[0])
            y.append(parsed_line[1])
    return XYData(x, y)


def main():
    file_contents = read_file('lica.xy')
    parsed = parse(file_contents)


if __name__ == '__main__':
    main()
