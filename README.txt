# Cerebrovascular Flow Analysis From 4D MRI
This is the primary repository for work undertaken as part of a Master of Engineering thesis.
The thesis is titled "Cerebrovascular Flow Analysis From 4D MRI" and was written by Katze Zambo.

The Gitlab group containing this repository can be found at:
https://gitlab.com/cbf-analysis-from-4dflow/

This repository contains mainly Python code, primarily for development of the neural network. There are also miscellaneous Python scripts for analysis, plot generation, etc.
It also contains a folder for thesis content (images, tables, etc).

All the files for Ansys Workbench (including geometry processing, meshing, simulation, simulation output) were originally in this repo. They can be seen in older commits. However as the project continued, the repo got too large, running into Gitlab's repository size limit. The Workbench files are now in the masters-cfd repo within this same Gitlab group.

Most of the commits have "astrid" as the name. This is me; this gitlab account was originally for private projects. I made every commit to this repository (and to the masters-cfd repo).

All Python code was written and executed in Python 3.8. I cannot guarantee scripts will have the same functionality if run with other versions.


# Repository Structure

This section lists whether a file or folder was
- created (by me). If a folder is marked as created then all its contents were created by me.
- modified
- unmodified
Most modified/unmodified files were from the 4DFlowNet repository, others such as images were sourced elsewhere.

Note that the following items are omitted from the following tree:
- __pycache__ folders
- .git folders
- .gitignore files
- licenses
- readmes

-4DFlowNet-master/
	- data/		created
	- src/
		- Network/
			- __init__.py		unmodified
			- h5util.py		unmodified
			- loss_utils.py		modified
			- h5util.py		unmodified
			- PatchGenerator.py		unmodified
			- PatchHandler3D.py		modified
			- SR4DFlowNet.py		modified
			- TrainerController.py		modified
			- utility.py		unmodified
		- pipeline-input/	created
		- prepare_data/
			- DicomData.py		modified
			- frequency_domain.py		modified
			- h5functions.py		modified
			- PatchData.py		modified
			- prepare_lowres_dataset.py		modified
			- prepare_mri_data.py		modified
			- prepare_patches.py		modified
		- utils/
			- __init__.py		unmodified
			- config_reader.py		created
			- format_velocity_csv.py		modified
			- h5utils.py		unmodified
			- ImageDataset.py		modified
			- interpolated_cfd.csv		created
			- pointcloud-mini.csv		created
			- prediction_utils.py		modified
			- velocity_csv_to_hdf5.py		modified
		- config.ini		created
		- generate_data.py		created
		- generte_noise_image.py		created
		- interpolated_csv.csv		created
		- multi_predictor.py	created
		- predictor.py		modified
		- test_iterator.py		modified
		- trainer.py		modified
- python/
	- epoch_logging/		created
	- epoch_logging_stripped/		created
	- multi_slice_data/		created
	- divergence_experimentation.py		created
	- epoch_logging.csv		created
	- flow_analysis.py		created
	- interpolated_cfd_LR.h5		created
	- multi_slice_viewer.py		modified
	- network_training_statistics.py		created
	- network-output-hr-to-sr.h5		created
	- network-output-lr-to-nr-new.h5		created
	- parse_xy.py		created
	- piecewise.py		created
	- s4_permutations.py		created
	- vtkwriter.py		modified
- thesis-content/
	- flow_analysis 		created
	- images-from-uni-comp1		created
	- images-from-uni-comp2		created
	- lucidchart		created
	- slicer		created
	- training		created
	- udfs		created
	- 0049_F4_MIP_COR.png		unmodified
	- 0049_F4_MIP_SAG.png		unmodified
	- 0049_F4_MIP_TRA.png		modified
	- circle_of_willis_wikimedia.png		unmodified
	- healthy_vs_severe_AD_image.jpg		unmodified
	- mesh_convergence.xlsx		created
	- nn_architecture_tikz.png		unmodified
	- vessel-measurements.tgn		created

# License

This repository is released under the MIT license.


# Contact Info

kzam437@aucklanduni.ac.nz