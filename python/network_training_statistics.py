import csv
from matplotlib import pyplot as plt
import matplotlib.ticker as mtick
from math import floor

def read_csv(path):
    rows = []

    with open(path) as file:
        reader = csv.DictReader(file)
        for row in reader:
            rows.append(row)
    return rows


def plot(x, y, y_axis_percentage=False):
    ymin = 2
    ymax = max(y) * 1.1
    plt.plot(x, y, c='red', ls='', ms=5, marker='.')
    ax = plt.gca()
    ax.set_ylim([ymin, ymax])
    ax.set_title('4DFlowNet validation accuracy versus training epoch')
    ax.set_ylabel('Validation accuracy')
    ax.set_xlabel('Epoch')
    if y_axis_percentage:
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
    plt.show()


def extract_row_elements(rows, element, type_fn):
    return [type_fn(row[element]) for row in rows]


def get_best_epoch(rows):
    best_epoch = 0
    for row in rows:
        try:
            if row[' best_model'] is not None:
                best_epoch = int(row['epoch'])
        except KeyError:
            continue
    return best_epoch


def plot_val_loss(rows):
    epochs = extract_row_elements(rows, 'epoch', int)
    val_accuracy = extract_row_elements(rows, 'val_accuracy', float)
    plot(epochs, val_accuracy, y_axis_percentage=True)


def plot_train_val_loss_best_model(input_file, model_title, y_axis_percentage=False, stop_at_best=True, invert_y=True):
    markersize = 20
    SMALL_SIZE = 8
    MEDIUM_SIZE = 14
    BIGGER_SIZE = 18

    plt.rc('font', size=MEDIUM_SIZE)  # controls default text sizes
    plt.rc('axes', titlesize=MEDIUM_SIZE)  # fontsize of the axes title
    plt.rc('axes', labelsize=MEDIUM_SIZE)  # fontsize of the x and y labels
    plt.rc('xtick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=MEDIUM_SIZE)  # fontsize of the tick labels
    plt.rc('legend', fontsize=MEDIUM_SIZE)  # legend fontsize
    plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title


    rows = read_csv(input_file)
    epochs = extract_row_elements(rows, 'epoch', int)
    val_accuracy = extract_row_elements(rows, 'val_accuracy', float)
    train_accuracy = extract_row_elements(rows, 'train_accuracy', float)
    best_epoch = get_best_epoch(rows)
    if stop_at_best:
        epochs = epochs[0:best_epoch]
        val_accuracy = val_accuracy[0:best_epoch]
        train_accuracy = train_accuracy[0:best_epoch]

    if invert_y:
        train_accuracy = [100 - epoch_train_acc for epoch_train_acc in train_accuracy]
        val_accuracy = [100 - epoch_val_acc for epoch_val_acc in val_accuracy]
        y_min = floor(min(val_accuracy + train_accuracy) - 1)
        y_max = 100
    else:
        y_max = max(val_accuracy + train_accuracy) * 1.1
        y_min = max(val_accuracy + train_accuracy) * 0.9


    plt.plot(epochs, train_accuracy, c='red', ls='', ms=markersize, marker='.',  label='Training')
    plt.plot(epochs, val_accuracy, c='blue', ls='', ms=markersize, marker='.', label='Validation')
    plt.legend(loc='upper right')
    ax = plt.gca()
    ax.set_ylim([y_min, y_max])
    ax.set_title('Training and validation loss versus training epoch ' + model_title)
    ax.set_ylabel('Accuracy (%)')
    ax.set_xlabel('Epoch')
    if y_axis_percentage:
        ax.yaxis.set_major_formatter(mtick.PercentFormatter())
    plt.show()


def main_velocity_only():
    input_file = 'epoch_logging_stripped/velocity-only-4p84.csv'
    plot_train_val_loss_best_model(input_file, '-- velocity loss only', y_axis_percentage=True)

def main_gradient_loss():
    input_file = 'epoch_logging_stripped/gradient-loss-2p46-1weight.csv'
    plot_train_val_loss_best_model(input_file, '-- velocity loss and gradient loss term', y_axis_percentage=True)

def main_divergence_loss():
    input_file = 'epoch_logging_stripped/divergence-loss-2p94-0p3weight.csv'
    plot_train_val_loss_best_model(input_file, '-- velocity loss and divergence loss term', y_axis_percentage=True)

def main():
    rows = read_csv('epoch_logging.csv')
    plot_val_loss(rows)


if __name__ == '__main__':
    # main_velocity_only()
    # main_gradient_loss()
    main_divergence_loss()


